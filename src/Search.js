import React from 'react';
import './Search.less';
import { MdSearch, MdSettingsVoice} from "react-icons/md";

const Search = () => {
  return (
    <section className='search'>
      <header>
        <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                   alt="Google Logo"/>
      </header>
      <section className='search-input'>
        <MdSearch className='search-icon'/>
        <input/>
        <MdSettingsVoice className='voice-icon'/>
        <button>Google搜索</button>
        <button>手气不错</button>
        <div className='remark'>
          Google提供:
          <nav className='remark-nav'>
            <a href={'#'}>中文（繁体）</a>
            <a href={'#'}>English</a>
          </nav>
        </div>
      </section>
    </section>
  );
};

export default Search;