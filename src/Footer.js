import React from "react";
import './Footer.less';

const Footer = () => {
    return (
        <footer>
            <div className={'location'}>
                香港
            </div>
            <hr/>
            <nav className='tools'>
                <a href={'https://ads.google.com/intl/zh_HK/home/'}>广告</a>
                <a href={'https://www.google.com.hk/services/#?modal_active=none'}>商务</a>
                <a href={'https://about.google/intl/zh-CN_hk/'}>Google大全</a>
                <a href={'https://www.google.com/search/howsearchworks/?fg=1'}>Google搜索的运作方式</a>
            </nav>
        </footer>
    );
};

export default Footer;

